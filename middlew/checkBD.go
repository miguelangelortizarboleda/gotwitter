package middlew

import (
	"github.com/zitrot/goTwitter/bd"
	"net/http"
)

func CheckBD(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if bd.CheckConnectiondb() != nil {
			http.Error(w, "conexion perdida con la base de datos", 500)
			return
		}
		next.ServeHTTP(w, r)
	}
}
