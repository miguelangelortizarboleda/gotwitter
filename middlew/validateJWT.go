package middlew

import (
	"net/http"

	"github.com/zitrot/goTwitter/routers"
)

func ValidateJWT(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, _, err := routers.ProcessToken(r.Header.Get("Autho"))

		if err != nil {
			http.Error(w, "Error in Token "+err.Error(), http.StatusBadRequest)
			return
		}
		next.ServeHTTP(w, r)
	}
}
