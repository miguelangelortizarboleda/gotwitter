package main

import (
	"log"

	"github.com/zitrot/goTwitter/handlers"

	"github.com/zitrot/goTwitter/bd"
)

func main() {

	err := bd.CheckConnectiondb()
	if err != nil {
		log.Println("Conexion Fallida")
	}

	handlers.Handlers()

}
