package models

import "time"

type User struct {
	Id        int64
	FirstName string
	LastName  string
	BirthDay  time.Time
	Email string
	Password string
	Avatar string
	Banner string
	Biopic string
	Location string
	WebSite string
}
