package bd

import (
	"github.com/zitrot/goTwitter/models"
)

func CheckUserExistsByEmail(email string) (bool, error) {
	var t models.User

	db, _ := Dbconnection()
	defer db.Close()
	err := db.QueryRow("SELECT email FROM user where email = ?", email).Scan(&t.Email)

	if err != nil {
		return false, err
	} else {
		return true, err
	}
}

func CheckUserExistsById(idUser string) (bool, error) {
	var t models.User

	db, _ := Dbconnection()
	defer db.Close()
	err := db.QueryRow("SELECT iduser FROM user where iduser = ?", idUser).Scan(&t.Id)

	if err != nil {
		return false, err
	} else {
		return true, err
	}
}
