package bd

import (
	"log"

	"github.com/zitrot/goTwitter/models"
)

func PutUser(u models.User, ID string) error {
	db, _ := Dbconnection()
	defer db.Close()
	query := "UPDATE user SET first_name = ?, last_name = ?, birth_day = ? ,email = ?,password = ?, avatar = ?, banner = ?, biopic = ?, location = ?, webside = ? WHERE iduser = ?"
	res, err := db.Exec(query, u.FirstName, u.LastName, u.BirthDay, u.Email, u.Password, u.Avatar, u.Banner, u.Biopic, u.Location, u.WebSite, ID)

	if err != nil {
		log.Printf("Error %s when preparing SQL statement", err)
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when finding rows affected", err)
		return err
	}
	log.Printf("%d user updated ", rows)
	return nil

}
