package bd

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func Dbconnection() (db *sql.DB, err error) {
	db, err = sql.Open("mysql", "root:Password<2@(127.0.0.1:3306)/test?parseTime=true")
	if err != nil {
		log.Fatal(err)
	}

	return
}

func CheckConnectiondb() error {
	db, err := Dbconnection()

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	log.Println("conexion exitosa")
	db.Close()

	return err
}
