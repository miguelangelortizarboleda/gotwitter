package bd

import (
	"log"

	"github.com/zitrot/goTwitter/models"
)

func GetUserbyEmail(email string) (models.User, error) {
	var user models.User
	db, _ := Dbconnection()
	defer db.Close()
	rows, err := db.Query("SELECT first_name, last_name, birth_day, email, password, avatar, banner, biopic, location, webside FROM user where email = ?", email)

	if err != nil {
		log.Printf("Error %s when preparing SQL statement", err)
		return user, err
	}
	defer rows.Close()

	users := make([]models.User, 0)
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.FirstName, &user.LastName, &user.BirthDay, &user.Email, &user.Password, &user.Avatar, &user.Banner, &user.Biopic, &user.Location, &user.WebSite); err != nil {
			log.Printf(err.Error())
			return user, err
		}
		users = append(users, user)
	}

	return users[0], err

}

func GetUserbyId(id string) (models.User, error) {
	var user models.User
	db, _ := Dbconnection()
	defer db.Close()
	rows, err := db.Query("SELECT first_name, last_name, birth_day, email, password, avatar, banner, biopic, location, webside FROM user where iduser = ?", id)
	if err != nil {
		log.Printf("Error %s when preparing SQL statement", err)
		return user, err
	}
	defer rows.Close()

	users := make([]models.User, 0)
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.FirstName, &user.LastName, &user.BirthDay, &user.Email, &user.Password, &user.Avatar, &user.Banner, &user.Biopic, &user.Location, &user.WebSite); err != nil {
			log.Printf(err.Error())
			return user, err
		}
		users = append(users, user)
	}

	return users[0], err

}
