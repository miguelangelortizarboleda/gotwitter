package bd

import (
	"log"

	"github.com/zitrot/goTwitter/models"
)

func InsertUser(u models.User) error {

	password, err := EncryptPassword(u.Password)
	if err != nil {
		return err
	}
	u.Password = password

	db, _ := Dbconnection()
	defer db.Close()
	query := "INSERT INTO user(first_name, last_name, birth_day ,email ,password, avatar, banner, biopic, location, webside) VALUES (?,?,?,?,?,?,?,?,?,?)"
	res, err := db.Exec(query, u.FirstName, u.LastName, u.BirthDay, u.Email, u.Password, u.Avatar, u.Banner, u.Biopic, u.Location, u.WebSite)
	if err != nil {
		log.Printf("Error %s when preparing SQL statement", err)
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when finding rows affected", err)
		return err
	}
	log.Printf("%d user created ", rows)
	return nil
}
