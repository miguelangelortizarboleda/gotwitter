package routers

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/zitrot/goTwitter/bd"
	"github.com/zitrot/goTwitter/jwt"
	"github.com/zitrot/goTwitter/models"
)

func LogIn(w http.ResponseWriter, r *http.Request) {
	var t models.User
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "error pasing the data"+err.Error(), 400)
		return
	}

	if len(t.Email) == 0 {
		http.Error(w, "Email missing", 400)
		return
	}

	if len(t.Password) == 0 {
		http.Error(w, "Password missing", 400)
		return
	}

	user, err := bd.GetUserbyEmail(t.Email)
	if err != nil {
		http.Error(w, "there was an error during the vemail verification", 400)
		return
	}

	if !bd.VerifyPassword(user.Password, t.Password) {
		http.Error(w, "The password is incorrect", 400)
		return
	}

	jwtKey, err := jwt.GenerateJWT(user)
	if err != nil {
		http.Error(w, "There was an error generating the token"+err.Error(), 400)
		return
	}

	resp := models.LoginResponse{
		Token: jwtKey,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
	expirationTime := time.Now().Add(24 * time.Hour)
	http.SetCookie(w, &http.Cookie{Name: "token", Value: jwtKey, Expires: expirationTime})

}
