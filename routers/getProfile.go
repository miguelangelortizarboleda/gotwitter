package routers

import (
	"encoding/json"
	"net/http"

	"github.com/zitrot/goTwitter/bd"
)

func GetProfile(w http.ResponseWriter, r *http.Request) {
	ID := r.URL.Query().Get("id")
	if len(ID) < 1 {
		http.Error(w, "you must send the ID parameter", http.StatusBadRequest)
		return
	}

	profile, err := bd.GetUserbyId(ID)
	if err != nil {
		http.Error(w, "there was an error during the vemail verification", 400)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(profile)

}
