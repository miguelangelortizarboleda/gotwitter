package routers

import (
	"errors"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/zitrot/goTwitter/bd"
	"github.com/zitrot/goTwitter/models"
)

var Email string

func ProcessToken(tk string) (*models.Claim, bool, error) {
	myKey := []byte("myspecialKeyjnljnlnñlmlknlnlknn")
	claims := &models.Claim{}

	splitToken := strings.Split(tk, "bearer")
	if len(splitToken) != 2 {
		return claims, false, errors.New("Invalid Token Format")
	}

	tk = strings.TrimSpace(splitToken[1])

	tkn, err := jwt.ParseWithClaims(tk, claims, func(token *jwt.Token) (interface{}, error) {
		return myKey, nil
	})
	if err == nil {
		found, _ := bd.CheckUserExistsByEmail(claims.Email)

		if found {
			Email = claims.Email
		}
		return claims, found, nil
	}
	if !tkn.Valid {
		return claims, false, errors.New("Invalid token")
	}

	return claims, false, err
}
