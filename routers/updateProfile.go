package routers

import (
	"encoding/json"
	"github.com/zitrot/goTwitter/bd"
	"github.com/zitrot/goTwitter/models"
	"net/http"
	"reflect"
	"time"
)

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	ID := r.URL.Query().Get("id")
	if len(ID) < 1 {
		http.Error(w, "you must send the ID parameter", http.StatusBadRequest)
		return
	}


	var t models.User
	err := json.NewDecoder(r.Body).Decode(&t)

	v := reflect.ValueOf(t)
	typeOfS := v.Type()

	date := time.Time{}

	for i := 0; i < v.NumField(); i++ {

		if typeOfS.Field(i).Name == "BirthDay" && v.Field(i).Interface() == date {
			 user, err := bd.GetUserbyId(ID)

			if err != nil {
				http.Error(w, "error getting user info "+err.Error(), 400)
				return
			}

			t.BirthDay = user.BirthDay
		}
	}

	if err != nil {
		http.Error(w, "error pasing the data"+err.Error(), 400)
		return
	}

	userFound, err := bd.CheckUserExistsById(ID)
	if !userFound {
		http.Error(w, "User don't exist", 400)
		return
	}

	err = bd.PutUser(t, ID)
	if err != nil {
		http.Error(w, "There was an error updating the user "+err.Error(), 400)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(t)
}
