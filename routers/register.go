package routers

import (
	"encoding/json"
	"net/http"

	"github.com/zitrot/goTwitter/bd"
	"github.com/zitrot/goTwitter/models"
)

func SignIn(w http.ResponseWriter, r *http.Request) {

	var t models.User
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "error pasing the data"+err.Error(), 400)
		return
	}

	if len(t.Email) == 0 {
		http.Error(w, "Email missing", 400)
		return
	}

	if len(t.Password) < 6 {
		http.Error(w, "Password must have at least 6 characters", 400)
		return
	}
	userFound, err := bd.CheckUserExistsByEmail(t.Email)
	if userFound {
		http.Error(w, "User already exist", 400)
		return
	}

	err = bd.InsertUser(t)

	if err != nil {
		http.Error(w, "error storing new user"+err.Error(), 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
