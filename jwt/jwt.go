package jwt

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/zitrot/goTwitter/models"
)

func GenerateJWT(t models.User) (string, error) {
	mYKey := []byte("myspecialKeyjnljnlnñlmlknlnlknn")

	payload := jwt.MapClaims{
		"email":     t.Email,
		"firstName": t.FirstName,
		"lastName":  t.LastName,
		"birthDay":  t.BirthDay,
		"biopic":    t.Biopic,
		"location":  t.Location,
		"webSite":   t.WebSite,
		"exp":       time.Now().Add(time.Hour * 24).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)

	tokenStr, err := token.SignedString(mYKey)
	if err != nil {
		return tokenStr, err
	}

	return tokenStr, nil

}
