package handlers

import (
	"log"
	"net/http"
	"os"

	"github.com/zitrot/goTwitter/middlew"
	"github.com/zitrot/goTwitter/routers"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func Handlers() {

	router := mux.NewRouter()

	router.HandleFunc("/signIn", middlew.CheckBD(routers.SignIn)).Methods("POST")

	router.HandleFunc("/login", middlew.CheckBD(routers.LogIn)).Methods("GET")

	router.HandleFunc("/get_profile", middlew.CheckBD(middlew.ValidateJWT(routers.GetProfile))).Methods("GET")

	router.HandleFunc("/update_profile", middlew.CheckBD(middlew.ValidateJWT(routers.UpdateProfile))).Methods("PUT")

	PORT := os.Getenv("PORT")

	if PORT == "" {
		PORT = "8080"
	}

	handler := cors.AllowAll().Handler(router)

	log.Fatal(http.ListenAndServe(":"+PORT, handler))

}
